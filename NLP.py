# %%

#  Import For Getting Web Article
from random import sample
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer
import os
from gensim.models.coherencemodel import CoherenceModel

import spacy
from gensim.models import CoherenceModel
from gensim.utils import simple_preprocess
import gensim.corpora as corpora
import gensim
import pandas as pd
import numpy as np
import pymongo
from pymongo import MongoClient

# import pyLDAvis.gensim_models as pyldavis
import pyLDAvis
import pyLDAvis.gensim
import pyLDAvis.sklearn

# Imports For WordCLoud
from wordcloud import WordCloud, STOPWORDS
import matplotlib.pyplot as plt

# Imports For Generating Uni/Bigrams
import nltk
from nltk import word_tokenize
from nltk.corpus import stopwords
from nltk.util import ngrams

import warnings
warnings.filterwarnings("ignore")

# For Sentiment Analyis Part
sid = SentimentIntensityAnalyzer()
# Creating the object for LDA model using gensim library
LDA = gensim.models.ldamodel.LdaModel


# Connect To Mongo Db
cluster = MongoClient(
    "mongodb+srv://anish:anish@nlppython.gmuox.mongodb.net/test?retryWrites=true&w=majority")
db = cluster['test']
collection = db["article"]
collection.drop()


# Setup For Opening File through Selenium
PATH = ".\chromedriver.exe"
driver = webdriver.Chrome(PATH)
driver.get("https://www.cnas.org/articles-multimedia")


# Get Word Cloud
def getWordCloud(article):
    wordcloud = WordCloud(width=800, height=800,
                          background_color='white',
                          stopwords=STOPWORDS,
                          min_font_size=10).generate(article)
    plt.figure(figsize=(8, 8), facecolor=None)
    plt.imshow(wordcloud)
    plt.axis("off")
    plt.tight_layout(pad=0)
    wordcloud.to_file("results/wordcloud/{}.png".format(id))
    return

# Get Tokenized Word


def getTokenizedWord(article):
    tokenized_sent = word_tokenize(article.lower())
    tokenized_sent_nostop = [
        token for token in tokenized_sent if token not in STOPWORDS]
    print(tokenized_sent_nostop)
    return tokenized_sent_nostop


# Get Unigrams/ Bigrams
def getBigrams(tokenized_sent_nostop):
    bigrams = ngrams(tokenized_sent_nostop, 2)
    bigrams = list(bigrams)
    print(bigrams)
    return bigrams

# Perfomm Sentiment analysis


def doSentimentAnalysis(bigrams):
    df = pd.DataFrame({'bigrams': bigrams})
    for sentence in bigrams:
        ss = sid.polarity_scores(sentence)
        df['neg'] = df['bigrams'].apply(
            lambda x: sid.polarity_scores(ss)['neg'])
        df['neu'] = df['bigrams'].apply(
            lambda x: sid.polarity_scores(ss)['neu'])
        df['pos'] = df['bigrams'].apply(
            lambda x: sid.polarity_scores(ss)['pos'])
        df['compound'] = df['bigrams'].apply(
            lambda x: sid.polarity_scores(ss)['compound'])
        df['comp_score'] = df['compound'].apply(
            lambda compound: 'pos' if compound >= 0 else 'neg')
    # print("Df HEAD", df)

    return df

# For Data Lemmatization


def lemmatization(texts, allowed_postags=['NOUN', 'ADJ']):
    nlp = spacy.load('en_core_web_sm', disable=['parser', 'ner'])
    output = []
    for sent in texts:
        doc = nlp(sent)
        output.append(
            [token.lemma_ for token in doc if token.pos_ in allowed_postags])
    return output


# Get  Article & Push into MongoDB
try:

    contents = WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.ID, "content"))
    )
    elements = contents.find_elements_by_xpath(
        "//*[@class='sans-serif fz16 bold margin-top-half-em']")

    linkArray = []
    for eachLink in elements:
        linkArray.append(eachLink.get_attribute("href"))

    # driver.close()

    linkArray = sample(linkArray, 5)
    print(">>Display All Links", linkArray)

    for link in linkArray:
        driver.get(link)
        article = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.ID, "mainbar"))
        ).text
        collection.insert_one({"article": article})
    driver.close()


# Get All Articles From Mongo DataBase
    getAllArtilces = collection.find({})
    getAllArtilces = list(getAllArtilces)

    # print(">>Total Articles From Database: len(getAllArtilces)", len(getAllArtilces))

# Iterate Article on Collected Articles
    for article in getAllArtilces:
        id = article["_id"]
        article = article["article"]
        print("Article ID", id)

# Generate Word Cloud
        getWordCloud(article)
        tokenized_sent_nostop = getTokenizedWord(article)
        bigrams = getBigrams(tokenized_sent_nostop)
        doSentimentAnalysis(bigrams)

# Sentiment analysis of whole text
        textSentimentAnalysis = SentimentIntensityAnalyzer().polarity_scores(article)
        print(">>Sentiment Analysis Of Whole Text", textSentimentAnalysis)

        for each in bigrams:
            bigramsSentimentAnalysis = SentimentIntensityAnalyzer().polarity_scores(each)
            # print("Bigrams", bigramsSentimentAnalysis)


# Perform Data Lematization
        tokenized_lem = lemmatization(tokenized_sent_nostop)
# Create Dictionary & Matrix
        dictionary = corpora.Dictionary(tokenized_lem)
        print(dictionary)
        doc_term_matrix = [dictionary.doc2bow(
            rev) for rev in tokenized_lem]
        # print("doc_term_matrix",doc_term_matrix)

# Build LDA model
        lda_model = LDA(corpus=doc_term_matrix, id2word=dictionary, num_topics=10, random_state=100,
                        chunksize=1000, passes=50, iterations=100)
        lda_model.print_topics()
        print(" lda_model.print_topics", lda_model.print_topics())

# For Coherence Score \ nPerplexity
        # coherence_model_lda = CoherenceModel(
        #     model=lda_model, texts=tokenized_lem, dictionary=dictionary, coherence='c_v')
        # print('\nPerplexity: ', lda_model.log_perplexity(
        #     doc_term_matrix, total_docs=10000))
        # coherence_lda = coherence_model_lda.get_coherence()
        # print('\nCoherence Score: ', coherence_lda)
        vis = pyLDAvis.gensim.prepare(lda_model, doc_term_matrix, dictionary)
        pyLDAvis.save_html(vis, 'results/lda/{}.html'.format(id))

    exit()


except:
    driver.quit()


# %%
